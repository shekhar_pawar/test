﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OdeToFood.Models;

namespace OdeToFood.Controllers
{
    public class HomeController : Controller
    {
        OdeToFoodDb _db = new OdeToFoodDb();


        public ActionResult Index()
        {
            var model = from r in _db.Restaurants
                        orderby r.Reviews.Average(review => review.Rating)
                        select new RestaurantListViewModel()
                        {
                            Id=r.Id,
                            Name=r.Name,
                            City=r.City,
                            Country=r.Country,
                            CountOfReviews = r.Reviews.Count()
                        };

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            ViewBag.Location = " Koperkhairane Navi Mumbai 400709";
            //return View();
            //if you want to use from created model
            var model = new AboutModel();
            model.Name = "Shekhar Pawar";
            model.Location = "Navi Mumbai";

            return View(model);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (_db != null)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
