﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OdeToFood.Filters;

namespace OdeToFood.Controllers
{
    public class CuisineController : Controller
    {
        //
        // GET: /Cuisine/

        //public ActionResult Index()
        //{
        //    return View();
        //}
       
        //if you want to get parameter from link, add parameter to action method Example(string name)
        //[HttpPost] // Adding attribute so that it can only invoke with http post methods
        //Example of Action Filter 
       [Log]
        public ActionResult Search(String name)
        {
            var message = Server.HtmlEncode(name);
            return Content(message);
        }
    }
}
