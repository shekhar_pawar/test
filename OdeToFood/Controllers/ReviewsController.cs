﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OdeToFood.Models;

//namespace OdeToFood.Controllers
//{
//    public class ReviewsController : Controller
//    {
//        //
//        // GET: /Reviews/
//        [ChildActionOnly]
//        public ActionResult BestReview()
//        {
//            var bestReview = from r in _review
//                             orderby r.Rating descending
//                             select r;

//            return PartialView("_Review", bestReview.First());
//        }

//        public ActionResult Index()
//        {
//            var model = from r in _review
//                        orderby r.Country
//                        select r;

//            return View(model);
//        }

//        //
//        // GET: /Reviews/Details/5

//        public ActionResult Details(int id)
//        {
//            return View();
//        }

//        //
//        // GET: /Reviews/Create

//        public ActionResult Create()
//        {
//            return View();
//        }

//        //
//        // POST: /Reviews/Create

//        [HttpPost]
//        public ActionResult Create(FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add insert logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        //
//        // GET: /Reviews/Edit/5

//        public ActionResult Edit(int id)
//        {
//            var review = _review.Single(r => r.Id == id);
//            return View(review);
//        }

//        //
//        // POST: /Reviews/Edit/5

//        [HttpPost]
//        public ActionResult Edit(int id, FormCollection collection)
//        {
//            var review = _review.Single(r => r.Id == id);
//            if (TryUpdateModel(review))
//            {
//                //... Database Action
//                return RedirectToAction("Index");
//            }
//            return View(review);
//        }

//        //
//        // GET: /Reviews/Delete/5

//        public ActionResult Delete(int id)
//        {
//            return View();
//        }

//        //
//        // POST: /Reviews/Delete/5

//        [HttpPost]
//        public ActionResult Delete(int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add delete logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        static List<RestaurantReview> _review = new List<RestaurantReview>
//        {
//            new RestaurantReview
//            {
//                Id= 1,
//                Name = "Kamal Vihar",
//                City= "Mumbai",
//                Country= "India",
//                Rating=6

//            },
//            new RestaurantReview
//            {
//                Id= 2,
//                Name = "Madras Cafe",
//                City= "Mumbai",
//                Country= "India",
//                Rating=8

//            },
//            new RestaurantReview
//            {
//                Id= 3,
//                Name = "Varishtha",
//                City= "Navi Mumbai",
//                Country= "India",
//                Rating=9

//            },
//            new RestaurantReview
//            {
//                Id= 4,
//                Name = "Zyca",
//                City= "Navi Mumbai",
//                Country= "India",
//                Rating=5

//            }
//        };
//    }
//}
