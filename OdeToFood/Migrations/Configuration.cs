namespace OdeToFood.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Collections;
    using OdeToFood.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<OdeToFood.Models.OdeToFoodDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(OdeToFood.Models.OdeToFoodDb context)
        {
            context.Restaurants.AddOrUpdate(r => r.Name,
                new Restaurant { Name = "Kamal Vihar", City = "Mumbai", Country = "India" },
                new Restaurant { Name = "Geeta Bhavan", City = "Chembur", Country = "India" },
                new Restaurant
                {
                    Name = "Campus",
                    City = "Mulund",
                    Country = "India",
                    Reviews = new List<RestaurantReview>
                    {new RestaurantReview { Rating=9,Body = "Great Food",ReviewerName="Mhashya"}           
                
                }
                });
        }
    }
}
